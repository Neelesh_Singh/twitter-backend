/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
  try {
    await knex.schema.createTable('user', (table) => {
      table.increments();
      table.string('firstName');
      table.string('lastName');
      table.string('userName').unique().notNullable();
      table.string('email').unique().notNullable();
      table.string('password').notNullable();

      table.index(['userName'], 'userNameIdx');
    });

    await knex.schema.createTable('tweet', (table) => {
      table.increments();
      table.string('content');
      table.string('userName');
      table.string('firstName');
      table.timestamp('created_at').defaultTo(knex.fn.now());
    });

    await knex.schema.createTable('follower-following', (table) => {
      table.increments();
      table
        .string('follower')
        .references('userName')
        .inTable('user')
        .notNullable();
      table
        .string('following')
        .references('userName')
        .inTable('user')
        .notNullable();

      table.index(['following'], 'followingIdx');
      table.index(['follower'], 'followerIdx');
    });

    return true;
  } catch (err) {
    console.log(err);
    return false;
  }
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {
  try {
    await knex.schema.dropTableIfExists('user-following');
    await knex.schema.dropTableIfExists('tweet');
    await knex.schema.dropTableIfExists('user-tweet');
    await knex.schema.dropTableIfExists('user');
    return true;
  } catch (err) {
    console.log(err);
    return false;
  }
};
