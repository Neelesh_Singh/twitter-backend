const { Model } = require('objection');
const database = require('../daatabaseConnection');
Model.knex(database);

module.exports = class Tweet extends Model {
  static get tableName() {
    return 'tweet';
  }
};
