const { Model } = require('objection');
const database = require('../daatabaseConnection');
Model.knex(database);

module.exports = class User_Following extends Model {
  static get tableName() {
    return 'follower-following';
  }
};
