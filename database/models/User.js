const { Model } = require('objection');
const database = require('../daatabaseConnection');
Model.knex(database);

module.exports = class User extends Model {
  static get tableName() {
    return 'user';
  }
};
