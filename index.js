const cors = require('cors');
const express = require('express');
const cookieParser = require('cookie-parser');
require('dotenv').config();

// Importing all the routes
const authRouter = require('./routes/authRoutes');
const tweetsRouter = require('./routes/tweetsRoutes');

const app = express();

// Middlewares
app.use(cors({ origin: 'http://localhost:3000', credentials: true }));
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ extended: true, limit: '50mb' }));
app.use(cookieParser());

// Routes
app.use('/api/v1/auth', authRouter);
app.use('/api/v1/tweets', tweetsRouter);
app.all('*', (req, res, next) => {
  res.status(404).json({ status: 'Fail', message: 'No route like this found' });
});

// Server
const port = process.env.PORT || 3001;
server = app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
