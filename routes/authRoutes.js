const express = require('express');
const protect = require('../utils/protect');
const authController = require('./../controllers/authController');

const router = express.Router();

router.route('/signup').post(authController.signUp);
router.route('/login').post(authController.logIn);
router.route('/follow').post(protect, authController.follow);
router.route('/unfollow').post(protect, authController.unFollow);
router.route('/user').post(protect, authController.userInfo);
router.route('/search').post(protect, authController.searchUser);

module.exports = router;
