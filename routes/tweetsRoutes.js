const express = require('express');
const tweetsController = require('./../controllers/tweetsController');
const protect = require('./../utils/protect');

const router = express.Router();

router.route('/all').get(protect, tweetsController.getTweets);
router.route('/create').post(protect, tweetsController.createTweet);

module.exports = router;
