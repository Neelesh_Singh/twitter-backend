const { promisify } = require('util');
const jwt = require('jsonwebtoken');
const catchAsync = require('./catchAsync');

const User = require('./../database/models/User');

// Checks if the user is logged in
module.exports = catchAsync(async (req, res, next) => {
  let token = null;
  if (req.cookies.jwt) {
    token = req.cookies.jwt;
  }
  if (!token) {
    return res.status(200).json({
      status: 'Failed',
      message: 'Please login first',
    });
  }
  const decoded = await promisify(jwt.verify)(token, process.env.SECRET);

  const currentUser = await User.query().findById(decoded.id);

  if (!currentUser) {
    return res.status(200).json({
      status: 'Failed',
      message: 'Please login first',
    });
  }
  req.user = currentUser;
  next();
});
