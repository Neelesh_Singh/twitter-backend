const catchAsync = require('./../utils/catchAsync');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const User = require('./../database/models/User');
const Tweet = require('./../database/models/Tweet');
const Follower_Following = require('./../database/models/Follower_Following');

const signToken = (id) => {
  return jwt.sign({ id }, process.env.SECRET, { expiresIn: '9d' });
};

// sends cookie
const sendToken = (user, statusCode, res) => {
  const token = signToken(user.id);
  const cookieOptions = {
    expires: new Date(Date.now() + 9 * 24 * 60 * 60 * 1000),
    path: '/',
    sameSite: 'none',
    secure: true,
  };
  res.cookie('jwt', token, cookieOptions);
  user.password = undefined;
  res.status(statusCode).json({
    status: 'Success',
    data: { user },
  });
};

// hashPassword
const hashPassword = async (password) => {
  return await bcrypt.hash(password, 12);
};

// checkPassword
const checkPassword = async (password, hashedPassword) => {
  return await bcrypt.compare(password, hashedPassword);
};

// SignUp controller
exports.signUp = catchAsync(async (req, res, next) => {
  let { firstName, lastName, userName, email, password } = req.body;
  if (!firstName || !lastName || !userName || !email || !password) {
    res.json({
      status: 'Failed',
      message: 'All required fields not provided',
    });
    return;
  }

  password = await hashPassword(password);

  const user = await User.query().insert({
    firstName,
    lastName,
    userName,
    email,
    password,
  });

  sendToken(user, 201, res);
});

// LogIn controller
exports.logIn = catchAsync(async (req, res, next) => {
  const { userName, password } = req.body;
  if (!userName || !password) {
    res.json({
      status: 'Failed',
      message: 'Invalid username or password',
    });
    return;
  }

  const user = await User.query().where('userName', userName);

  if (user.length === 0) {
    res.json({
      status: 'Failed',
      message: 'Invalid username or password',
    });
  }

  if (!(await checkPassword(password, user[0].password))) {
    res.json({
      status: 'Failed',
      message: 'Invalid username or password',
    });
  }

  sendToken(user[0], 200, res);
});

exports.follow = catchAsync(async (req, res, next) => {
  const { userName } = req.body;

  await Follower_Following.query().insert({
    follower: req.user.userName,
    following: userName,
  });

  res.json({
    status: 'Success',
  });
});

exports.unFollow = catchAsync(async (req, res, next) => {
  const { userName } = req.body;

  await Follower_Following.query()
    .delete()
    .where('following', userName)
    .where('follower', req.user.userName);

  res.json({
    status: 'Success',
  });
});

exports.userInfo = catchAsync(async (req, res, next) => {
  const { userName } = req.body;

  const user = await User.query()
    .select(['userName', 'firstName', 'lastName'])
    .where('userName', userName);

  const following = await User.query()
    .select(['userName', 'firstName', 'lastName'])
    .whereIn(
      'userName',
      Follower_Following.query().select('following').where('follower', userName)
    );

  const followers = await User.query()
    .select(['userName', 'firstName', 'lastName'])
    .whereIn(
      'userName',
      Follower_Following.query().select('follower').where('following', userName)
    );

  const tweets = await Tweet.query()
    .where('userName', userName)
    .orderBy('created_at', 'DESC');

  res.json({
    status: 'Success',
    user,
    followers,
    following,
    tweets,
  });
});

exports.searchUser = catchAsync(async (req, res, next) => {
  const { term } = req.body;

  let result;

  if (term === '*') result = await User.query().limit(5);
  else result = await User.query().whereRaw(`"userName" like '${term}%'`);

  res.json({
    status: 'Success',
    result,
  });
});
