const catchAsync = require('./../utils/catchAsync');

const Tweet = require('./../database/models/Tweet');
const Follower_Following = require('../database/models/Follower_Following');
const User = require('../database/models/User');

// Returns all the posts in the database
exports.getTweets = catchAsync(async (req, res, next) => {
  const page = req.body.page || 1;
  const limit = req.body.limit || 10;

  // select every tweet whose follower is user or whose 'userName' is user
  const tweets = await Tweet.query()
    .whereIn(
      'userName',
      Follower_Following.query()
        .select('following')
        .where('follower', req.user.userName)
    )
    .orWhere('userName', req.user.userName)
    .offset((page - 1) * limit)
    .limit(limit)
    .orderBy('created_at', 'DESC');

  // used for follow unfollow in frontend
  const following = await User.query()
    .select(['userName'])
    .whereIn(
      'userName',
      Follower_Following.query()
        .select('following')
        .where('follower', req.user.userName)
    );

  res.json({ status: 'Success', tweets, following });
});

// Creates Post
exports.createTweet = catchAsync(async (req, res, next) => {
  const { content } = req.body;
  if (!content) {
    res.json({
      status: 'Failed',
      message: 'Post should have content',
    });
    return;
  }

  const { userName, firstName } = req.user;

  const post = await Tweet.query().insert({
    userName,
    firstName,
    content,
  });

  res.status(200).json({
    status: 'Success',
    post: post,
  });
});
